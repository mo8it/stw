use std::{borrow::Cow, env, path::PathBuf};

fn err<T>(msg: impl Into<Cow<'static, str>>) -> Result<T, Cow<'static, str>> {
    Err(msg.into())
}

fn symlink(src: PathBuf, dest: PathBuf) -> Result<(), Cow<'static, str>> {
    if std::os::unix::fs::symlink(&src, &dest).is_err() {
        return err(format!(
            "Failed to create the symlink {} -> {}",
            src.display(),
            dest.display()
        ));
    }

    println!("{} -> {}", src.display(), dest.display());

    Ok(())
}

fn walk(
    src_dir: PathBuf,
    dest_dir: PathBuf,
    excludes: Option<&'static [&'static str]>,
) -> Result<(), Cow<'static, str>> {
    'outer: for entry in src_dir.read_dir().unwrap() {
        let entry = entry.unwrap().path();

        if let Some(excludes) = excludes {
            for exclude in excludes {
                if entry.ends_with(exclude) {
                    continue 'outer;
                }
            }
        }

        let entry_postfix = entry.strip_prefix(&src_dir).unwrap();
        let dest = dest_dir.join(entry_postfix);

        if dest.is_symlink() {
            let Ok(link) = dest.canonicalize()
            else {
                return err(format!("Failed to canonicalize the symbolic link {}", dest.display()));
            };

            if link != entry {
                return err(format!(
                    "{} is a symlink pointing to {} instead of {}",
                    dest.display(),
                    link.display(),
                    entry.display()
                ));
            }
        } else if dest.is_dir() {
            if entry.is_dir() {
                walk(entry, dest, None)?;
            } else {
                return err(format!(
                    "The destination {} is a directory but the source {} is not a directory!",
                    dest.display(),
                    entry.display(),
                ));
            }
        } else if dest.exists() {
            return err(format!(
                "The destination {} already exists and it is neither a symlink nor a directory!",
                dest.display(),
            ));
        } else {
            symlink(entry, dest)?;
        }
    }

    Ok(())
}

fn main() -> Result<(), Cow<'static, str>> {
    let Ok(home_var) = env::var("HOME") else {
        return err("Failed to read the HOME environment variable!");
    };

    let home = PathBuf::from(home_var);

    walk(home.join(".dotfiles"), home, Some(&[".git", ".gitignore"]))
}
